const http = require('http')
const fs = require('fs')

function serveStaticFile(res, path, contentType, responseCode) {
    if (!responseCode) responseCode = 200
    fs.readFile(__dirname + path, (err, data) => {
        if (err) {
            res.writeHead(500, {'Content-type': 'text/plain'})
            res.end('500 - Internal Error')
        } else {
            res.writeHead(responseCode, {'Content-type': contentType})
            res.end(data)
        }
    })
}

http.createServer( (req, res) => {
    // res.writeHead(200, {'Content-type': 'text/plain'})
    // res.end('Hello world!')
    // 规范化url,去掉查询字符串，可选反斜杠，并把它变成小写
    var path = req.url.replace(/\/?(?:\?.*)?$/, '').toLowerCase()
    switch(path) {
        case '':
            // res.writeHead(200, {'Content-type': 'text/plain'})
            // res.end('Homepage')
            serveStaticFile(res, '/public/home.html', 'text/html')
            break
        case '/about':
            // res.writeHead(200, {'Content-type': 'text/plain'})
            // res.end('About')
            serveStaticFile(res, '/public/about.html', 'text/html')
            break
        case '/img/logo.jpg':
            serveStaticFile(res, '/public/img/logo.png', 'image/jpeg')
            break
        default:
            // res.writeHead(404, {'Content-type': 'text/plain'})
            // res.end('Not Found')
            serveStaticFile(res, '/public/404.html', 'text/html', 404)
            break
    }
}).listen(3000)

console.log('Server started on localhost:3000; press Ctrl-C to terminate...')